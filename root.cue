package cue

import (
	"github.com/fristonio/kit/cluster-agent/api/v1alpha1"
	"k8s.io/api/core/v1"
)

DefaultClusterConfig: v1alpha1.#K8sCluster & {
	spec: foo: "temp"
}

_clusterConfig: v1alpha1.#K8sCluster & DefaultClusterConfig

k8snamespace: helloWorld: v1.#Namespace & {
    apiVersion: "v1"
    kind: "Namespace"
    metadata: name: "hello-world"
}

configmap: derived: v1.#ConfigMap & {
	apiVersion: "v1"
	kind:       "ConfigMap"
	metadata: {
		name: _clusterConfig.spec.foo
        namespace: k8snamespace.helloWorld.metadata.name
		labels: component: "testing"
	}
	data: yyyy: "yyyy"
}

configmap: helloWorld: v1.#ConfigMap & {
	apiVersion: "v1"
	kind:       "ConfigMap"
	metadata: {
		name: _clusterConfig.spec.foo
        namespace: "default"
		labels: component: "testing"
	}
	data: xxxx: "xxxxx"
}

K8sPreExecutionResources: [
    k8snamespace.helloWorld,
]

K8sResources: [
	configmap.helloWorld,
    configmap.derived,
]

K8sPostExecutionResources:[]
